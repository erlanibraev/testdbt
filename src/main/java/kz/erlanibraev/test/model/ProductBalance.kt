package kz.erlanibraev.test.model

import kz.erlanibraev.test.repository.entities.ProductEntity
import java.math.BigDecimal

data class ProductBalance (
        val product: ProductEntity,
        val qtyStock: BigDecimal,
        val qtySold: BigDecimal,
        val cost: BigDecimal
)