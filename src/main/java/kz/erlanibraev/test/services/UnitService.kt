package kz.erlanibraev.test.services

import kz.erlanibraev.test.repository.entities.UnitEntity

interface UnitService {

    fun getUnits(): ArrayList<UnitEntity>

    fun save(entity: UnitEntity): UnitEntity
}