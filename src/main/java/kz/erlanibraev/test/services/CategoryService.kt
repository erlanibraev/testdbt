package kz.erlanibraev.test.services

import kz.erlanibraev.test.repository.entities.CategoryEntity

interface CategoryService {
    fun getCategory(): ArrayList<CategoryEntity>
    fun save(entity: CategoryEntity): CategoryEntity
}