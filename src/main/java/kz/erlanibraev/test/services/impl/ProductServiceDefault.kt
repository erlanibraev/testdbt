package kz.erlanibraev.test.services.impl

import kz.erlanibraev.test.repository.ProductRepository
import kz.erlanibraev.test.repository.entities.CategoryEntity
import kz.erlanibraev.test.repository.entities.ProductEntity
import kz.erlanibraev.test.repository.entities.UnitEntity
import kz.erlanibraev.test.services.ProductService
import org.springframework.stereotype.Service

@Service
class ProductServiceDefault(val productRepository: ProductRepository) : ProductService {
    override fun getProducts(): ArrayList<ProductEntity> =
            productRepository.findAll().toCollection(ArrayList<ProductEntity>())

    override fun filteredProducts(category: CategoryEntity?, unit: UnitEntity?): ArrayList<ProductEntity> =
            ArrayList(category?.let {
                unit?.let {
                    productRepository.findByCategoryAndUnit(category, unit)
                } ?: let {
                    productRepository.findByCategory(category)
                }
            } ?: let {
                unit?.let {
                    productRepository.findByUnit(unit)
                } ?: let {
                    productRepository.findAll()
                }
            })

    override fun save(entity: ProductEntity): ProductEntity =
            productRepository.save(entity)
}