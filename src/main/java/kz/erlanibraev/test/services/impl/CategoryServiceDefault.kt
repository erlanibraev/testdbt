package kz.erlanibraev.test.services.impl

import kz.erlanibraev.test.repository.CategoryRepository
import kz.erlanibraev.test.repository.entities.CategoryEntity
import kz.erlanibraev.test.services.CategoryService
import org.springframework.stereotype.Service

@Service
class CategoryServiceDefault(val categoryRepository: CategoryRepository): CategoryService {
    override fun getCategory(): ArrayList<CategoryEntity> =
            categoryRepository.findAll().toCollection(ArrayList<CategoryEntity>())

    override fun save(entity: CategoryEntity): CategoryEntity =
            categoryRepository.save(entity)
}