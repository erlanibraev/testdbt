package kz.erlanibraev.test.services.impl

import kz.erlanibraev.test.repository.UnitRepository
import kz.erlanibraev.test.repository.entities.UnitEntity
import kz.erlanibraev.test.services.UnitService
import org.springframework.stereotype.Component

@Component
class UnitServiceDefault(val unitRepository: UnitRepository): UnitService {

    override fun getUnits(): ArrayList<UnitEntity> =
            unitRepository.findAll().toCollection(ArrayList<UnitEntity>())

    override fun save(entity: UnitEntity): UnitEntity =
            unitRepository.save(entity)
}