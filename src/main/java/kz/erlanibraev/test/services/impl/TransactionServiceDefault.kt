package kz.erlanibraev.test.services.impl

import kz.erlanibraev.test.model.ProductBalance
import kz.erlanibraev.test.repository.TransactionRepository
import kz.erlanibraev.test.repository.entities.ProductEntity
import kz.erlanibraev.test.repository.entities.TransactionEntity
import kz.erlanibraev.test.services.TransactionService
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class TransactionServiceDefault(val transactionRepository: TransactionRepository): TransactionService {
    override fun creditBalance(): ArrayList<ProductBalance> =
            transactionRepository.findAll().groupBy { transaction ->
                transaction.product
            }.mapValues { entry ->
                val qtyStock = entry.value.fold(BigDecimal.ZERO) { acc, entity -> acc + entity.qty }.setScale(2, BigDecimal.ROUND_HALF_DOWN)
                val qtySold = entry.value.fold(BigDecimal.ZERO) { acc, entity -> acc + if (entity.qty < BigDecimal.ZERO) entity.qty.negate() else BigDecimal.ZERO }.setScale(2, BigDecimal.ROUND_HALF_DOWN)
                val cost = entry.value.fold(BigDecimal.ZERO) { acc, entity -> acc + if(entity.qty < BigDecimal.ZERO) entity.qty.negate() * entity.cost else BigDecimal.ZERO }.setScale(2, BigDecimal.ROUND_HALF_DOWN)
                ProductBalance(
                        product = entry.key!!,
                        cost = cost,
                        qtyStock = qtyStock,
                        qtySold = qtySold
                )
            }.values.toCollection(ArrayList<ProductBalance>())

    override fun getQty(entity: ProductEntity): BigDecimal =
            transactionRepository.sumQty(entity)

    override fun debetBalance(): ArrayList<ProductBalance> =
        transactionRepository.findAll().groupBy {transaction ->
            transaction.product
        }.mapValues { entry ->
            val qtyStock = entry.value.fold(BigDecimal.ZERO) { acc, entity -> acc + entity.qty }.setScale(2, BigDecimal.ROUND_HALF_DOWN)
            val qtySold = entry.value.fold(BigDecimal.ZERO) { acc, entity -> acc + if (entity.qty < BigDecimal.ZERO) entity.qty.negate() else BigDecimal.ZERO }.setScale(2, BigDecimal.ROUND_HALF_DOWN)
            val avgCost = BigDecimal.valueOf(entry.value.mapNotNull { product-> if(  product.qty > BigDecimal.ZERO) product.cost.toDouble() else null }.average()).setScale(2, BigDecimal.ROUND_HALF_DOWN)
            val cost = (avgCost * qtyStock).setScale(2, BigDecimal.ROUND_HALF_DOWN)
            ProductBalance(
                    product = entry.key!!,
                    cost = cost,
                    qtyStock = qtyStock,
                    qtySold = qtySold
            )
        }.values.toCollection(ArrayList<ProductBalance>())

    override fun save(entity: TransactionEntity): TransactionEntity =
            transactionRepository.save(entity)
}