package kz.erlanibraev.test.services

import kz.erlanibraev.test.model.ProductBalance
import kz.erlanibraev.test.repository.entities.ProductEntity
import kz.erlanibraev.test.repository.entities.TransactionEntity
import java.math.BigDecimal

interface TransactionService {

    fun debetBalance(): ArrayList<ProductBalance>
    fun creditBalance(): ArrayList<ProductBalance>
    fun getQty(entity: ProductEntity): BigDecimal
    fun save(entity: TransactionEntity): TransactionEntity

}