package kz.erlanibraev.test.services

import kz.erlanibraev.test.repository.entities.CategoryEntity
import kz.erlanibraev.test.repository.entities.ProductEntity
import kz.erlanibraev.test.repository.entities.UnitEntity

interface ProductService {
    fun getProducts(): ArrayList<ProductEntity>
    fun filteredProducts(category: CategoryEntity?, unit: UnitEntity?): ArrayList<ProductEntity>
    fun save(entity: ProductEntity): ProductEntity
}