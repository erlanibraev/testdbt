package kz.erlanibraev.test.config

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.provisioning.InMemoryUserDetailsManager

@Configuration
@EnableWebSecurity
open class SecurityConfig: WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        try {
            http.csrf().disable()
            http
                    .userDetailsService(userDetailsService())
                    .authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/**.jsf").permitAll()
                    .antMatchers("/javax.faces.resource/**").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .formLogin()
                    .loginPage("/login.jsf")
                    .permitAll()
                    .failureUrl("/login.jsf?error=true")
                    .defaultSuccessUrl("/starter.jsf")
                    .and()
                    .logout()
                    .logoutSuccessUrl("/login.jsf")
                    .deleteCookies("JSESSIONID")
        } catch (ex: Exception) {
            throw RuntimeException(ex)
        }

    }

    override fun userDetailsService(): UserDetailsService {
        val result = InMemoryUserDetailsManager()
        result.createUser(User.withDefaultPasswordEncoder().username("admin").password("admin").authorities("ROLE_ADMIN").build())
        result.createUser(User.withDefaultPasswordEncoder().username("user").password("user").authorities("ROLE_USER").build())
        return result
    }
}