package kz.erlanibraev.test.config

import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
@Configuration
open class WelcomePageRedirectConfig: WebMvcConfigurer {

    override fun addViewControllers(registry: ViewControllerRegistry) {
        registry.apply {
            addViewController("/").setViewName("forward:/starter.xhtml")
            setOrder(Ordered.HIGHEST_PRECEDENCE)
        }
    }
}