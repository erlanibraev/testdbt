package kz.erlanibraev.test.config

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@EnableJpaRepositories(basePackages = ["kz.erlanibraev.test.repository"])
@EntityScan(basePackages = ["kz.erlanibraev.test.repository.entities"])
open class AppConfig {

}