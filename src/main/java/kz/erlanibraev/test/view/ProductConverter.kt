package kz.erlanibraev.test.view

import kz.erlanibraev.test.repository.entities.ProductEntity
import org.springframework.stereotype.Component
import javax.faces.view.ViewScoped

@Component("productConverter")
@ViewScoped
open class ProductConverter: AbstractConverter<ProductEntity>(ProductEntity::class.java) {

}