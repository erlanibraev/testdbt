package kz.erlanibraev.test.view

import kz.erlanibraev.test.model.ProductBalance
import kz.erlanibraev.test.repository.entities.TransactionEntity
import kz.erlanibraev.test.services.TransactionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.math.BigDecimal
import javax.annotation.PostConstruct
import javax.faces.context.FacesContext
import javax.faces.view.ViewScoped

@Component("debetMBean")
@ViewScoped
open class DebetMBean {

    @Autowired
    lateinit var transactionService: TransactionService

    private var transaction: TransactionEntity = TransactionEntity()
    private var total: BigDecimal = BigDecimal(0)
    private var balance: ArrayList<ProductBalance> = ArrayList()

    @PostConstruct
    open fun initData() {
        balance = transactionService.debetBalance()
    }

    fun getTransaction(): TransactionEntity = transaction
    fun setTransaction(value: TransactionEntity) {
        transaction = value
    }

    fun getTotal(): BigDecimal = total
    fun setTotal(value: BigDecimal) {
        total = value
    }

    fun getBalance(): ArrayList<ProductBalance> = balance
    fun setBalance(value: ArrayList<ProductBalance>) {
        balance = value
    }

    fun addTransaction() {
        transaction.user = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser()
        transactionService.save(transaction)
        balance = transactionService.debetBalance()
        transaction = TransactionEntity()
    }


}