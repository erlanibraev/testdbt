package kz.erlanibraev.test.view

import kz.erlanibraev.test.repository.entities.CategoryEntity
import org.springframework.stereotype.Component
import javax.faces.view.ViewScoped

@Component("categoryConverter")
@ViewScoped
open class CategoryConverter: AbstractConverter<CategoryEntity>(CategoryEntity::class.java) {

}