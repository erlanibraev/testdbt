package kz.erlanibraev.test.view

import com.google.gson.Gson
import javax.faces.component.UIComponent
import javax.faces.context.FacesContext
import javax.faces.convert.Converter

abstract class AbstractConverter<T>(private val clazz: Class<T>): Converter<T> {
    private val gson = Gson()

    override fun getAsObject(p0: FacesContext?, p1: UIComponent?, p2: String?): T =
            gson.fromJson(p2, clazz)

    override fun getAsString(p0: FacesContext?, p1: UIComponent?, p2: T): String =
            gson.toJson(p2)
}