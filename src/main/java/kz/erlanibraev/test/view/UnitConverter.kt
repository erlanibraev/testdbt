package kz.erlanibraev.test.view

import kz.erlanibraev.test.repository.entities.UnitEntity
import org.springframework.stereotype.Component
import javax.faces.view.ViewScoped

@Component("unitConverter")
@ViewScoped
open class UnitConverter: AbstractConverter<UnitEntity>(UnitEntity::class.java) {

}