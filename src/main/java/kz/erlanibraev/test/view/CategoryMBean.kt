package kz.erlanibraev.test.view

import kz.erlanibraev.test.repository.entities.CategoryEntity
import kz.erlanibraev.test.services.CategoryService
import org.primefaces.event.CellEditEvent
import org.springframework.stereotype.Component
import java.io.Serializable
import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.faces.context.FacesContext
import javax.faces.application.FacesMessage
import org.primefaces.event.RowEditEvent
import org.springframework.beans.factory.annotation.Autowired

@ViewScoped
@Component("categoryMBean")
open class CategoryMBean: Serializable {

    @Autowired
    lateinit var categoryService: CategoryService

    private lateinit var categoryList: ArrayList<CategoryEntity>

    fun getCategoryList(): ArrayList<CategoryEntity> = categoryList
    fun setCategoryList(value: ArrayList<CategoryEntity>) {
        categoryList = value
    }

    @PostConstruct
    fun initData() {
        categoryList = categoryService.getCategory()
    }

    fun onRowEdit(event: RowEditEvent) {
        val category = categoryService.save(event.`object` as CategoryEntity)
        val msg = FacesMessage("Редактирование категории", category!!.name)
        FacesContext.getCurrentInstance().addMessage(null, msg)
    }

    fun onRowCancel(event: RowEditEvent) {
        val msg = FacesMessage("Отмена редактирования", (event.getObject() as CategoryEntity).id.toString())
        FacesContext.getCurrentInstance().addMessage(null, msg)
    }

    fun onAddNew() {
        val max : Long = categoryList.maxBy { it.id!! }?.let { it.id!! + 1L } ?: 1L
        val category = categoryService.save(CategoryEntity())
        categoryList.add(category)
        setCategoryList(categoryList)
        val msg = FacesMessage("добавлена категория", category.id.toString())
        FacesContext.getCurrentInstance().addMessage(null, msg)
    }
}
