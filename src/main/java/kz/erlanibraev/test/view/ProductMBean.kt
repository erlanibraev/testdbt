package kz.erlanibraev.test.view

import kz.erlanibraev.test.repository.entities.CategoryEntity
import kz.erlanibraev.test.repository.entities.ProductEntity
import kz.erlanibraev.test.repository.entities.UnitEntity
import kz.erlanibraev.test.services.ProductService
import org.primefaces.event.RowEditEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.context.FacesContext
import javax.faces.view.ViewScoped


@ViewScoped
@Component("productMBean")
open class ProductMBean {

    @Autowired
    lateinit var productService: ProductService

    private lateinit var productList: ArrayList<ProductEntity>

    fun getProductList(): ArrayList<ProductEntity> = productList
    fun setProductList(value: ArrayList<ProductEntity>) {
        productList = value
    }

    @PostConstruct
    open fun initData() {
        productList = productService.getProducts()
    }

    fun onRowEdit(event: RowEditEvent) {
        val product = productService.save((event.getObject() as ProductEntity))
        val msg = FacesMessage("Редактирование продукта", product.name)
        FacesContext.getCurrentInstance().addMessage(null, msg)
    }

    fun onRowCancel(event: RowEditEvent) {
        val msg = FacesMessage("Отмена редактирования", (event.getObject() as ProductEntity).id.toString())
        FacesContext.getCurrentInstance().addMessage(null, msg)
    }

    fun onAddNew() {
        val max : Long = productList.maxBy { it.id!! }?.let { it.id!! + 1L } ?: 1L
        val product = productService.save(ProductEntity( ))
        productList.add(product)
        val msg = FacesMessage("добавлена категория", product.id.toString())
        FacesContext.getCurrentInstance().addMessage(null, msg)
    }
}