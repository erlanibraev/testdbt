package kz.erlanibraev.test.view

import kz.erlanibraev.test.repository.entities.UnitEntity
import kz.erlanibraev.test.services.UnitService
import org.primefaces.event.RowEditEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.Serializable
import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.context.FacesContext
import javax.faces.view.ViewScoped

@ViewScoped
@Component("unitMBean")
open class UnitMBean: Serializable {

    @Autowired
    lateinit var unitService: UnitService

    private lateinit var unitList: ArrayList<UnitEntity>


    fun getUnitList(): ArrayList<UnitEntity> = unitList
    fun setUnitList(value: ArrayList<UnitEntity>) {
        unitList = value
    }


    @PostConstruct
    fun initData() {
        unitList = unitService.getUnits()
    }

    fun onRowEdit(event: RowEditEvent) {

        val entity: UnitEntity = unitService.save(event.getObject() as UnitEntity)

        val id = (event.getObject() as UnitEntity).id

        val unit = unitList.find { it.id == id }

        val msg = FacesMessage("Редактирование ед. изм.", unit!!.name)
        FacesContext.getCurrentInstance().addMessage(null, msg)
    }

    fun onRowCancel(event: RowEditEvent) {
        val msg = FacesMessage("Отмена редактирования", (event.getObject() as UnitEntity).id.toString())
        FacesContext.getCurrentInstance().addMessage(null, msg)
    }

    fun onAddNew() {

        val unit = UnitEntity()
        unitList.add(unitService.save(unit))

        setUnitList(unitList)
        val msg = FacesMessage("добавлена категория", unit.id.toString())
        FacesContext.getCurrentInstance().addMessage(null, msg)
    }
}
