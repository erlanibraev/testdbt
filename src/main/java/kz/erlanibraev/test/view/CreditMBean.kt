package kz.erlanibraev.test.view

import kz.erlanibraev.test.model.ProductBalance
import kz.erlanibraev.test.repository.entities.TransactionEntity
import kz.erlanibraev.test.services.TransactionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.math.BigDecimal
import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.context.FacesContext
import javax.faces.view.ViewScoped

@Component
@ViewScoped
class CreditMBean {
    @Autowired
    lateinit var transactionService: TransactionService

    private var transaction: TransactionEntity = TransactionEntity()
    private var balance: ArrayList<ProductBalance> = ArrayList()

    @PostConstruct
    open fun initData() {
        balance = transactionService.creditBalance()
    }

    fun getTransaction(): TransactionEntity = transaction
    fun setTransaction(value: TransactionEntity) {
        transaction = value
    }

    fun getBalance(): ArrayList<ProductBalance> = balance
    fun setBalance(value: ArrayList<ProductBalance>) {
        balance = value
    }

    fun addTransaction() {

        val qty = transactionService.getQty(transaction.product!!) - transaction.qty
        if(qty >= BigDecimal.ZERO) {
            transaction.user = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser()
            transaction.qty = transaction.qty.negate()
            transactionService.save(transaction)
            balance = transactionService.creditBalance()
            transaction = TransactionEntity()
        } else {
            val msg = FacesMessage("Нельзя продать больше, чем есть на складе", "Сейчас: ${qty}, продаете ${transaction.qty}")
            FacesContext.getCurrentInstance().addMessage(null, msg)
        }
    }
}