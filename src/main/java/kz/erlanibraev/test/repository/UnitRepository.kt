package kz.erlanibraev.test.repository

import kz.erlanibraev.test.repository.entities.UnitEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface UnitRepository: JpaRepository<UnitEntity, Long> {

    fun findByNameLike(name: String): List<UnitEntity>
    fun findByNameLike(name: String, pageable: Pageable):Page<UnitEntity>

}