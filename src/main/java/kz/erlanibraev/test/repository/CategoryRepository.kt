package kz.erlanibraev.test.repository

import kz.erlanibraev.test.repository.entities.CategoryEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface CategoryRepository: JpaRepository<CategoryEntity, Long> {

    fun findByNameLike(name: String): List<CategoryEntity>
    fun findByNameLike(name: String, pageable:Pageable): Page<CategoryEntity>

}