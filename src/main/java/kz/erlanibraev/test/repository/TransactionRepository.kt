package kz.erlanibraev.test.repository

import kz.erlanibraev.test.repository.entities.ProductEntity
import kz.erlanibraev.test.repository.entities.TransactionEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.math.BigDecimal

interface TransactionRepository: JpaRepository<TransactionEntity, Long> {
    @Query(value = """
select  sum(t.qty) from TransactionEntity t where t.product = :product        
    """)
    fun sumQty(@Param("product") productEntity: ProductEntity): BigDecimal
}