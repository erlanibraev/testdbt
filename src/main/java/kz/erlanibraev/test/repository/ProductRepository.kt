package kz.erlanibraev.test.repository

import kz.erlanibraev.test.repository.entities.CategoryEntity
import kz.erlanibraev.test.repository.entities.ProductEntity
import kz.erlanibraev.test.repository.entities.UnitEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface ProductRepository: JpaRepository<ProductEntity, Long> {

    fun findByCategory(category: CategoryEntity): List<ProductEntity>
    fun findByCategory(category: CategoryEntity, pageable: Pageable): Page<ProductEntity>

    fun findByUnit(unit:UnitEntity): List<ProductEntity>
    fun findByUnit(unit:UnitEntity, pageable: Pageable): Page<ProductEntity>

    fun findByCategoryAndUnit(category: CategoryEntity, unit: UnitEntity): List<ProductEntity>
    fun findByCategoryAndUnit(category: CategoryEntity, unit: UnitEntity, pageable: Pageable): Page<ProductEntity>

    fun findByNameLike(name: String): List<ProductEntity>
    fun findByNameLike(name: String, pageable: Pageable): Page<ProductEntity>

    fun findByCategoryAndNameLike(category: CategoryEntity, name: String): List<ProductEntity>
    fun findByCategoryAndNameLike(category: CategoryEntity, name: String, pageable: Pageable): Page<ProductEntity>

}