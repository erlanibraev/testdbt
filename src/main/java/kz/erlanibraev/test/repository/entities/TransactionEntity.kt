package kz.erlanibraev.test.repository.entities

import org.jetbrains.annotations.NotNull
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name="transaction", schema="test")
data class TransactionEntity(
        @Id
        @Column(name="id")
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name="product_id")
        var product: ProductEntity? = null,
        @Column(name="qty", precision = 12, scale = 2)
        var qty: BigDecimal = BigDecimal(0),
        @Column(name="cost", precision = 12, scale = 2)
        var cost: BigDecimal = BigDecimal(0),
        @Column(name="user_name", length = 1024)
        @NotNull
        var user: String? = null,
        @Column(name="created")
        var created: LocalDate = LocalDate.now()
)