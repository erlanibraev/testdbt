package kz.erlanibraev.test.repository.entities

import javax.persistence.*

@Entity
@Table(name="product", schema = "test", uniqueConstraints = [UniqueConstraint(columnNames = ["name", "category_id", "unit_id"])])
data class ProductEntity(
        @Id
        @Column(name="id")
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        @Column(name="name", length = 1024)
        var name: String? = null,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name="category_id" )
        var category: CategoryEntity? = null,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name="unit_id")
        var unit: UnitEntity? = null
)