package kz.erlanibraev.test.repository.entities

import javax.persistence.*

@Entity
@Table(name="category", schema = "test", uniqueConstraints = [UniqueConstraint(columnNames = ["name"])])
data class CategoryEntity(
        @Id
        @Column(name="id")
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        @Column(name="name", length = 1024)
        var name: String? = null
)


