package kz.erlanibraev.test.services

import io.mockk.every
import io.mockk.mockk
import kz.erlanibraev.test.repository.TransactionRepository
import kz.erlanibraev.test.repository.entities.CategoryEntity
import kz.erlanibraev.test.repository.entities.ProductEntity
import kz.erlanibraev.test.repository.entities.TransactionEntity
import kz.erlanibraev.test.repository.entities.UnitEntity
import kz.erlanibraev.test.services.impl.TransactionServiceDefault
import org.junit.Test
import java.math.BigDecimal
import kotlin.test.assertEquals

class TransactionServiceTest {
    val categories: List<CategoryEntity> = listOf(
            CategoryEntity(
                    id = 1,
                    name = "Категория 1"
            ),
            CategoryEntity(
                    id = 2,
                    name = "Категория 2"
            ),
            CategoryEntity(
                    id = 3,
                    name = "Категория 3"
            )
    )

    val units: List<UnitEntity> = listOf(
            UnitEntity(
                    id = 1,
                    name = "Ед. Изм. 1"
            ),
            UnitEntity(
                    id = 2,
                    name = "Ед. Изм. 2"
            ),
            UnitEntity(
                    id = 3,
                    name = "Ед. Изм. 3"
            )
    )

    val products: List<ProductEntity> = listOf(
            ProductEntity(
                    id = 1,
                    category = categories[0],
                    unit = units[0],
                    name = "Продукт 1"
            ),
            ProductEntity(
                    id = 2,
                    category = categories[1],
                    unit = units[0],
                    name = "Продукт 2"
            ),
            ProductEntity(
                    id = 3,
                    category = categories[2],
                    unit = units[0],
                    name = "Продукт 3"
            ),
            ProductEntity(
                    id = 4,
                    category = categories[0],
                    unit = units[1],
                    name = "Продукт 4"
            ),
            ProductEntity(
                    id = 5,
                    category = categories[1],
                    unit = units[1],
                    name = "Продукт 5"
            ),
            ProductEntity(
                    id = 6,
                    category = categories[2],
                    unit = units[1],
                    name = "Продукт 6"
            ),
            ProductEntity(
                    id = 7,
                    category = categories[0],
                    unit = units[2],
                    name = "Продукт 7"
            ),
            ProductEntity(
                    id = 8,
                    category = categories[1],
                    unit = units[2],
                    name = "Продукт 8"
            ),
            ProductEntity(
                    id = 9,
                    category = categories[2],
                    unit = units[2],
                    name = "Продукт 9"
            )
    )

    val transactions: List<TransactionEntity> = listOf(
            TransactionEntity(
                    id = 1,
                    product = products[0],
                    qty = BigDecimal.valueOf(100.0),
                    cost = BigDecimal.valueOf(10.0)
            ),
            TransactionEntity(
                    id = 2,
                    product = products[0],
                    qty = BigDecimal.valueOf(-100.0),
                    cost = BigDecimal.valueOf(100.0)
            ),
            TransactionEntity(
                    id = 3,
                    product = products[0],
                    qty = BigDecimal.valueOf(100.0),
                    cost = BigDecimal.valueOf(10.0)
            ),
            TransactionEntity(
                    id = 2,
                    product = products[1],
                    qty = BigDecimal.valueOf(2.0),
                    cost = BigDecimal.valueOf(10.0)
            ),
            TransactionEntity(
                    id = 2,
                    product = products[1],
                    qty = BigDecimal.valueOf(3.0),
                    cost = BigDecimal.valueOf(3.0)
            )
    )

    @Test
    fun testDebetBalance() {
        val transactionRepository: TransactionRepository = mockk()
        every {
            transactionRepository.findAll()
        } returns transactions

        val transactionService = TransactionServiceDefault(transactionRepository)
        val balance = transactionService.debetBalance()
        assertEquals(balance[0].qtyStock, BigDecimal.valueOf(100.00).setScale(2))
        assertEquals(balance[0].cost, BigDecimal.valueOf(1000.00).setScale(2))
        assertEquals(balance[1].qtyStock, BigDecimal.valueOf(5.00).setScale(2))
        assertEquals(balance[1].cost, BigDecimal.valueOf(32.50).setScale(2))
    }

    @Test
    fun testCreditBalance() {
        val transactionRepository: TransactionRepository = mockk()
        every {
            transactionRepository.findAll()
        } returns transactions

        val transactionService = TransactionServiceDefault(transactionRepository)
        val balance = transactionService.creditBalance()
        assertEquals(balance[0].qtySold, BigDecimal.valueOf(100.00).setScale(2))
        assertEquals(balance[0].cost, BigDecimal.valueOf(10000.00).setScale(2))
        assertEquals(balance[1].qtySold, BigDecimal.valueOf(0.0).setScale(2))
        assertEquals(balance[1].cost, BigDecimal.valueOf(0.0).setScale(2))

    }
}